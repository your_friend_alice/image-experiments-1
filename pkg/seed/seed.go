package seed

import (
	mathrand "math/rand"
	cryptorand "crypto/rand"
	"encoding/binary"
	"bytes"
)

func Seed() {
	buf := make([]byte, 8)
	cryptorand.Read(buf)
	seed, err := binary.ReadVarint(bytes.NewBuffer(buf))
	if err != nil {
		panic(err.Error())
	}
	mathrand.Seed(seed)
}
