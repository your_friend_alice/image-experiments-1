package x

import (
	"github.com/BurntSushi/xgbutil"
	"github.com/BurntSushi/xgbutil/xgraphics"

	"image"
)

func ScreenGeometry() (int, int, error) {
	x, err := xgbutil.NewConn()
	defer x.Conn().Close()
	if err != nil {
		return 0, 0, err
	}
	screen := x.Screen()
	return int(screen.WidthInPixels), int(screen.HeightInPixels), nil
}

func SetDesktop(img image.Image) error {
	x, err := xgbutil.NewConn()
	defer x.Conn().Close()
	if err != nil {
		return err
	}
	window := x.RootWin()
	ximg := xgraphics.NewConvert(x, img)
	defer ximg.Destroy()
	ximg.XSurfaceSet(window)
	ximg.XDraw()
	ximg.XPaint(window)
	return nil
}
