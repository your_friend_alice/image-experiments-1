package main

import (
	"github.com/fogleman/gg"
	"gitlab.com/your_friend_alice/image-experiments-1/pkg/seed"
	"gitlab.com/your_friend_alice/image-experiments-1/pkg/x"
	"image"
	"log"
	"math/rand"
	"time"
)

// margin-[top|left|right|bottom] = 20
// [horizontal|vertical]-align = center
// grid-size = 50

func main() {
	seed.Seed()
	delay := 50 * time.Millisecond
	probability := 0.1
	width, height, err := x.ScreenGeometry()
	if err != nil {
		log.Fatal(err)
	}
	context := gg.NewContext(width, height)
	context.SetHexColor(Backgrounds[1])
	context.Clear()
	err = x.SetDesktop(context.Image())
	time.Sleep(delay)
	err = x.SetDesktop(DoSomeArt(context, 1, false))
	if err != nil {
		log.Fatal(err)
	}
	for {
		delay += delay / 10
		if delay > 60*time.Second {
			delay = 60 * time.Second
		}
		probability *= 0.95
		if probability < 0.001 {
			probability = 0.001
		}
		time.Sleep(delay)
		err = x.SetDesktop(DoSomeArt(context, probability, true))
		if err != nil {
			log.Fatal(err)
		}
	}
}

var Backgrounds = []string{"073642", "002b36"}

var Foregrounds = []string{"dc322f", "859900", "b58900", "268bd2", "d33682", "2aa198", "eee8d5"}

func DoSomeArt(context *gg.Context, coverage float64, foreground bool) image.Image {
	width := float64(context.Width())
	height := float64(context.Height())
	if coverage == 1 {
		context.SetHexColor("002b36")
		context.Clear()
	}
	grid := 50.0
	for x := grid / 2; x < width-grid; x += grid {
		for y := grid / 2; y < height-grid; y += grid {
			if rand.Float64() <= coverage {
				if y < height-2*grid && y > grid {
					backgroundAt(context, x, y, grid, Backgrounds)
				} else {
					backgroundAt(context, x, y, grid, []string{Backgrounds[1]})
				}
				if foreground {
					DoSomeArtAt(context, x, y, grid)
				}
			}
		}
	}
	return context.Image()
}

func DoSomeArtAt(context *gg.Context, x, y, grid float64) {
	switch rand.Intn(5) {
	case 0:
		circleAt(context, x, y, grid)
	case 1:
		squareAt(context, x, y, grid)
	case 2:
		halfSquareAt(context, x, y, grid)
	}
}

func randomColor(context *gg.Context, set []string) {
	r := rand.Intn(len(set))
	color := set[r]
	context.SetHexColor(color)
}

func backgroundAt(context *gg.Context, x, y, grid float64, set []string) {
	randomColor(context, set)
	context.DrawRectangle(x, y, grid, grid)
	context.Fill()
}
func circleAt(context *gg.Context, x, y, grid float64) {
	radiusDiff := rand.Float64()*0.1 + 0.1
	middleRadius := rand.Float64()*(0.6-radiusDiff) + 0.3

	randomColor(context, Foregrounds)
	context.DrawCircle(x+grid/2, y+grid/2, (middleRadius+radiusDiff)*grid/2)
	context.Fill()

	if rand.Intn(3) == 1 {
		randomColor(context, Foregrounds)
		context.DrawCircle(x+grid/2, y+grid/2, (middleRadius-radiusDiff)*grid/2)
		context.Fill()
	}
}

func squareAt(context *gg.Context, x, y, grid float64) {
	randomColor(context, Foregrounds)
	context.DrawRectangle(x, y, grid, grid)
	context.Fill()
	if rand.Intn(3) == 1 {
		randomColor(context, Backgrounds)
		context.DrawCircle(x+grid/2, y+grid/2, (rand.Float64()*0.6+0.2)*grid/2)
		context.Fill()
	}
}

func halfSquareAt(context *gg.Context, x, y, grid float64) {
	randomColor(context, Foregrounds)
	points := [][]float64{{0, 0}, {0, 1}, {1, 0}, {1, 1}}
	excludePoint := rand.Intn(4)
	for i, point := range points {
		if i != excludePoint {
			context.LineTo(x+grid*point[0], y+grid*point[1])
		}
	}
	context.Fill()
}
